package jp.alhinc.springtraining.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.EditUserForm;
import jp.alhinc.springtraining.mapper.UserMapper;

@Service
public class EditUserService {

@Autowired
private UserMapper mapper;

@Transactional
public User findEditUser(Long id) {
	return mapper.findEditUser(id);
}

@Transactional
public int edit(EditUserForm form,User entity) {
	//登録する情報
	entity.setName(form.getName());
	entity.setLogin_id(form.getLogin_id());
	entity.setBranch_id(form.getBranch_id());
	entity.setPosition_id(form.getPosition_id());
	entity.setStatus(Byte.valueOf("1"));

	//パスワード暗号化処理
	if(form.getRawPassword() != null) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		entity.setPassword(encoder.encode(form.getRawPassword()));
	}
	//DB登録
	return mapper.update(entity);
	}

	@Transactional
	public int update(User entity) {
		return mapper.update(entity);
	}
}
