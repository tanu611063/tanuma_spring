package jp.alhinc.springtraining.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class GetAllBranchesService {

	public Map<Integer, String> getAllBranches(){
		Map<Integer, String> branch = new HashMap<>();
			branch.put(1, "本社");
			branch.put(2, "支店A");
			branch.put(3, "支店B");
			branch.put(4, "支店C");
			return branch;
		}
}