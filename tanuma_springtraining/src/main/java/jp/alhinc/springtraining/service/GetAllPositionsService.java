package jp.alhinc.springtraining.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class GetAllPositionsService {

	@Transactional
	public Map<Integer, String> getAllPositions(){
		Map<Integer, String> Position = new HashMap<>();
		Position.put(1, "総務人事担当者");
		Position.put(2, "情報管理担当者");
		Position.put(3, "支店長");
		Position.put(4, "社員1");
		Position.put(5, "社員2");
		Position.put(6, "社員3");
		return Position;
	}
}
