package jp.alhinc.springtraining.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.CreateUserForm;
import jp.alhinc.springtraining.form.EditUserForm;
import jp.alhinc.springtraining.service.CreateUserService;
import jp.alhinc.springtraining.service.EditUserService;
import jp.alhinc.springtraining.service.GetAllBranchesService;
import jp.alhinc.springtraining.service.GetAllPositionsService;
import jp.alhinc.springtraining.service.GetAllUsersService;

@Controller
@RequestMapping("/users")
public class UsersController {

	@Autowired
	private GetAllUsersService getAllUsersService;

	@Autowired
	private CreateUserService createUserService;

	@Autowired
    private GetAllBranchesService getAllBranchesService;

	@Autowired
    private GetAllPositionsService getAllPositionsService;

	@Autowired
    private EditUserService editUserService;

	@Autowired
	private HttpSession session;

	//ユーザー一覧画面表示
	@GetMapping
	public String index(Model model) {
		//全ユーザー情報取得
		List<User> users = getAllUsersService.getAllUsers();
		model.addAttribute("users", users);
		return "users/index";
	}

	//新規登録画面表示(GET=取得)
	@GetMapping("/create")
	public String create(CreateUserForm form, Model model) {
		form.setBranchMap(getAllBranchesService.getAllBranches());
		form.setPositionMap(getAllPositionsService.getAllPositions());
        model.addAttribute("form", form);
		return "users/create";
	}

	//ユーザー新規登録(POST=登録、更新)
	@PostMapping("/create")
	public String create(@Validated @ModelAttribute("form") CreateUserForm form, BindingResult result, Model model) {

//		if (CreateUserService.searchUpdateLoginId(form, login_id)) {
//			// resultにエラーを追加する
//			ObjectError fieldError = new ObjectError("loginIdErrorMessage", "すでに登録されたログインIDです");
//			result.addError(fieldError);
//			// エラーメッセージを格納
//			model.addAttribute("loginIdErrorMessage", "すでに登録されたログインIDです");
//		}

		if (result.hasErrors()) {
			//エラー
			model.addAttribute("message", "残念");
			form.setBranchMap(getAllBranchesService.getAllBranches());
	        form.setPositionMap(getAllPositionsService.getAllPositions());
			model.addAttribute("form", form);
			return "/users/create";
		}
		//登録完了
		createUserService.create(form);
		return "redirect:/users";
	}

	 @GetMapping("/edit/{id}")
	 public String edit(@PathVariable Long id, EditUserForm form, Model model) {
		 User user = editUserService.findEditUser(id);
	     form.setId(id);
	     form.setLogin_id(user.getLogin_id());
	     form.setName(user.getName());
	     form.setBranch_id(user.getBranch_id());
	     form.setPosition_id(user.getPosition_id());
	     form.setBranchMap(getAllBranchesService.getAllBranches());
	     form.setPositionMap(getAllPositionsService.getAllPositions());
	     model.addAttribute("form", form);
	     session.setAttribute("user",user);
	     return "users/edit";
	  }

	 @PostMapping("/update")
	 public String update(@Validated @ModelAttribute("form") EditUserForm form, BindingResult result, Model model) {
	    if (result.hasErrors()) {
	    	form.setBranchMap(getAllBranchesService.getAllBranches());
	    	form.setPositionMap(getAllPositionsService.getAllPositions());
	    	model.addAttribute("form", form);
	    	return "/users/edit";
	    }
	    User user = (User)session.getAttribute("user");
	    editUserService.edit(form,user);
	    return "redirect:/users";
	 }

	 @InitBinder
	 public void initBinder(WebDataBinder binder) {
		 // 未入力のStringをnullに設定する
		 binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	 }

	 @GetMapping("/stop/{id}")
	 public String stop(@PathVariable Long id, Model model) {
		 User user = editUserService.findEditUser(id);
	     if(user == null) {
	    	 return "redirect:/users";
	     }
	     user.setStatus(Byte.valueOf("0"));
	     editUserService.update(user);
	     return "redirect:/users";
	 }

	 @GetMapping("/restart/{id}")
	  public String restart(@PathVariable Long id, Model model) {
		 User user = editUserService.findEditUser(id);
	     if(user == null) {
	    	 return "redirect:/users";
	     }
	     user.setStatus(Byte.valueOf("1"));
	     editUserService.update(user);
	     return "redirect:/users";
	 }
}
